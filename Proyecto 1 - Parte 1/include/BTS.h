#pragma once

#include <string>       //string
#include <iostream>     //cin, std::cout

using namespace std;

template <typename Datum>
class Node{
  public:
    Node(){};
    Node(Datum d){
      data = d;
    };
    ~Node(){};

    Datum data;
    Node<Datum>* hijod;
    Node<Datum>* hijoi;
    Node<Datum>* ancestro;

    Datum getVal(){
      return data;
    };
    void setVal(Datum d){
      data = d;
    };
};

template <typename Datum>
class BST{
  public:

    Node<Datum> *raiz;
    BST<Datum> *izquierda;
    BST<Datum> *derecha;


    bool isNotBalanced = true;  //arbol debe estar balanceado

    BST(){
      raiz = nullptr;
      izquierda = nullptr;
      derecha = nullptr;
    };

    BST(Datum d){
      raiz = new Node<Datum>(d);
      izquierda = nullptr;
      derecha = nullptr;
    };

    ~BST(){};

/**
 @brief Funcion para insertar nodos
 */
      void insert(Datum d){
      if(raiz == NULL) { // Primer elemento
          raiz = new Node<Datum>(d);
          raiz->hijod = nullptr;
          raiz->hijoi = nullptr;
      }

  
      else {
          // Si el valor es más grande que la raíz y la raíz no tiene hijo derecho, inserta a la derecha
          if(raiz->getVal() < d) {
              if(derecha == NULL) {
                  derecha = new BST<Datum>(d);
                  raiz->hijod = derecha->raiz;
                  (raiz->hijod)->ancestro = raiz;
              }
              // Si la derecha no está vacía, recorre el subárbol
              else {
                  derecha->insert(d);
              }
          }

          // Recorrido hacia el subárbol de la izquierda
          else {
              if(raiz->getVal() > d) {
                  if(izquierda == NULL) {
                      izquierda = new BST<Datum>(d);
                      raiz->hijoi = izquierda->raiz;
                      (raiz->hijoi)->ancestro = raiz;
                  }
                  else {
                      izquierda->insert(d);
                  }
              }
          }
      }
    };

/**
 @brief Funcion para eliminar nodos
 */
    void removeNode(Datum d){
        Node<Datum>* dNode = find(d); // Se busca el nodo a eliminar
        
        /// Si el nodo existe
        if (dNode != nullptr){

            /// Define el padre
            Node<Datum>* padre = dNode->ancestro;

            /// Si el nodo a eliminar no tiene hijos, solo se cambia el puntero de su padre y se elimina
            if ( (dNode->hijod == nullptr) && (dNode->hijoi == nullptr)){

                if (padre->hijoi == dNode){
                    padre->hijoi = nullptr; 
                } else {
                    padre->hijod = nullptr;
                }

                delete dNode;
                

            }

            // Si tiene un hijo, se hace un enlace entre su padre y su hijo y luego se elimina
            if ( (dNode->hijod != nullptr) && (dNode->hijoi == nullptr) ){

                if (padre->hijoi == dNode){
                    padre->hijoi = dNode->hijod;
                } else {
                    padre->hijod = dNode->hijod;
                }

                delete dNode;
                
            }

            if ( (dNode->hijod == nullptr) && (dNode->hijoi != nullptr) ){

                if (padre->hijoi == dNode){
                    padre->hijoi = dNode->hijoi;
                } else {
                    padre->hijod = dNode->hijoi;
                }

                delete dNode;
                
            }

            /** Si tiene dos hijos, se busca el elemento más pequeño hacia la derecha, se intercambian los valores del nodo a 
             * eliminar con el hallado y se elimina el nodo pequeño */
            if ( (dNode->hijod != nullptr) && (dNode->hijoi != nullptr) ){
                Node<Datum>* menor = findSmallestToTheRight(dNode);
                Datum dd = menor -> data;
                removeNode(menor->data);
                dNode -> data = dd;
            }
        }
    }


/**
 @brief Funcion para encontrar un elemento
 */
    Node<Datum>* find(Datum d){
        Node<Datum>* copia = this->raiz; //Copia del valor de la raiz
        while(copia != nullptr && copia->data != d){ 

            // Elemento a buscar  es menor que la raíz va hacia la izquierda
            if(d < copia->getVal()){

                copia = copia->hijoi;

            // Si es mayor a la derecha
            } else{
                copia = copia->hijod;
            }
        }

        if (copia != nullptr){
            return copia;
        } else {
            return nullptr;
        }
    };



 
  

/**
 @brief Funcion para encontrar el elemento mas grande a la izq, Primero indica la primera entrada
 */
    Node<Datum> * auxFindLargestToTheLeft(Node<Datum> * n, string ind){
        Node<Datum> * r;

        // Verifica que no sea el nodo de entrada, o sea que se entre al subárbol de la izquierda
        if (ind == "Primero" && n->hijoi != nullptr){
            n = n->hijoi;
        }
       
        if ((n->hijod) != nullptr){
            r = this -> auxFindLargestToTheLeft(n->hijod, "Primero"); // Llamado recursivo 
        }

        // Si no hay subárbol derecho, se retorna el nodo
        else
            r = n;
        return r;
    }

/**
 @brief Funcion para encontrar el elemento mas grande a la izq, Primero indica la primera entrada
 */
    Node <Datum>* findLargestToTheLeft(Node<Datum>* n){
        return auxFindLargestToTheLeft(n, "Primero");
    }

 /**
 @brief Funcion para encontrar el elemento mas pequeno a la derecha
 */
    Node<Datum> * auxFindSmallestToTheRight(Node<Datum> * n, string ind){
        Node<Datum> * r;
        if (ind == "Primero" && n->hijod != nullptr){
            n = n->hijod;
        }
        if ((n->hijoi) != nullptr){
            r = this -> auxFindSmallestToTheRight(n->hijoi, "");
        }
        else
            r = n;
        return r;
    }

/**
 @brief Funcion para encontrar el elemento mas pequeno a la derecha
 */
    Node <Datum>* findSmallestToTheRight(Node<Datum>* n){
        return auxFindSmallestToTheRight(n, "Primero");
    }



/**
 @brief Funcion para acomodar inorden el arbol
 */
    void auxInOrden(Node<Datum>* node){ 
        if (node != nullptr){ //Si el nodo no es vacío, imprime
            if (node->hijoi != NULL) {
                auxInOrden(node->hijoi); // Llamado recursivo 
            }

            cout << node->getVal() << ", "; /// Imprime la raíz

            // Llamado recursivo 
            if (node->hijod != NULL) {
                auxInOrden(node->hijod);
            }
        }
    }
/**
 @brief Funcion para acomodar inorden el arbol
 */
    void inOrden(){
        auxInOrden(this->raiz);
    }


/**
 @brief Funcion para acomodar preorden el arbol
 */

    void auxPreOrden(Node<Datum>* node){
        if (node != nullptr){
            cout << node->getVal() << ", ";

            if (node->hijoi != NULL) {
                auxPreOrden(node->hijoi);
            }

            if (node->hijod != NULL) {
                auxPreOrden(node->hijod);
            }
        }
    }
/**
 @brief Funcion para acomodar preorden el arbol
 */
 
    void preOrden(){
        auxPreOrden(this->raiz);
    }
/**
 @brief Funcion para acomodar posorden el arbol
 */
 
    void auxPosOrden(Node<Datum>* node){

        if (node != nullptr){
            if (node->hijoi != NULL) {
                auxPosOrden(node->hijoi);
            }

            if (node->hijod != NULL) {
                auxPosOrden(node->hijod);
            }

            cout << node->getVal() << ", ";
        }
    
    }

/**
 @brief Funcion para acomodar posorden el arbol
 */
    void posOrden(){
        auxPosOrden(this->raiz);
    }
/**
 @brief Funcion para imprimir el arbol
 */

void printBST(string prefix, Node<Datum>* node, bool isLeft){
    
    /// Si el árbol no está vacío, imprime
    if( node != nullptr ){
        cout << prefix;

        cout << (isLeft ? "├──" : "└──" );

        /// Imprime el valor del nodo
        cout << node->getVal() << endl;

        /// Baja al siguiente nivel del árbol
        if (node->hijoi != nullptr){
            printBST( prefix + (isLeft ? "│   " : "    "), node->hijoi, true);
        }
        if (node->hijod != nullptr){
            printBST( prefix + (isLeft ? "│   " : "    "), node->hijod, false);
        }
    }
}


};

