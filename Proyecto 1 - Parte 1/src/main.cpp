#include "BTS.h"


int main(){
    BST<int> arbol;

   //Prueba   
  
   arbol.insert(20);
   arbol.insert(10);
   arbol.insert(5);
   arbol.insert(15);
   arbol.insert(1);
   arbol.insert(7);
   arbol.insert(30);
   arbol.insert(25);
   arbol.insert(35);
   arbol.insert(32);
   arbol.insert(40);



   

   cout<<"Esto es una prueba del funcionamiento de los metodos del arbol binario"<<endl;
   cout << "Árbol :" << endl;
    arbol.printBST("", arbol.raiz, false);
   
    cout << "El numero mas grande  a la izquierda es :" << endl;
    cout << arbol.findLargestToTheLeft(arbol.raiz)->getVal() << endl;

    cout << "El número mas pequeno  a la derecha es :" << endl;
    cout << arbol.findSmallestToTheRight(arbol.raiz)->getVal() << endl;


    cout << "Impresion preOrden: ";
    arbol.preOrden();
    cout << endl;

    cout << "Impresion posOrden: ";
    arbol.posOrden();
    cout << endl;

    cout << "Impresion inOrden: ";
    arbol.inOrden();
    cout << endl;

   cout<<"Ahora vamos a eliminar un  nodo: el numero 10" <<endl;
   arbol.removeNode(10);

    cout << "Árbol despues de eliminar el 10 :" << endl;
    arbol.printBST("", arbol.raiz, false);



    
    return 0;
}
