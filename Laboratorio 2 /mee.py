import sys
umbral=int(sys.argv[1])
text=sys.argv[2]
def cargarArchivo(nombre): #cargamos el archivo
  archivo = open (nombre,'r')
  contenido = archivo.read()
  archivo.close()
  return contenido

def guardarArchivo(contenido, nombre): #guardamos el archivo
  archivo = open (nombre,'w+')
  archivo.write(contenido)
  archivo.close()

def detectorUmbral(umbral, lista):  #el diccionario lo creamos para hacer la tabla
  diccionario = {}
  cont = 1
  for palabra in lista:
    if lista.count(palabra) >= umbral:
      codigo = "@" + str(cont)
      diccionario.update({palabra : codigo})
      cont += 1
  return diccionario
def guardarTabla(diccionario, nombre): #guardar la tabla en el .tab
  texto = ""
  for key in diccionario:
    texto += key + " : " + diccionario.get(key) + "\n"
    guardarArchivo(texto, nombre)
def algoritmo(umbral,archivo):
  texto = cargarArchivo(archivo)
  lista = texto.split()
  print(lista) #probando la lista
  diccionario = detectorUmbral(umbral, lista)
  print(diccionario) #ver la tabla
  nuevoTexto = ""
  #Cambia las palabras y crea el nuevo texto
  for i in range(0,len(lista)):
    if diccionario.get(lista[i]) != None:
      lista[i] = diccionario.get(lista[i])
    nuevoTexto += (lista[i] + " ")
  archivoNuevo = archivo.split(".")
  guardarArchivo(nuevoTexto, (archivo[0] + ".rep"))
  guardarTabla(diccionario, archivo[0]+ '.tab')
algoritmo(umbral,text)
