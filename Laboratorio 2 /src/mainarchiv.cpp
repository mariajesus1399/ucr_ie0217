#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include <stdio.h>
#include <string.h>
#include <cstdlib>
#include <vector>
#include "abrirarchivo.h"
using namespace std;
#include<map>

carchivo* c=new carchivo();
int main(int argc, char *argv[])

{
    string *x; //Definicion del array donde se almacenaran todas las palabras
    string archivo =argv[2]; //En archivo se encuentra el nombre del archivo a abrir
    int npalabras = 1;
    if (argc!=3)
    {
        cout<< "Debe ingresar más parámetros" << endl;
    }
    else
    { 
            int umbral;
            umbral = atoi(argv[1]); //En umbral se tiene el umbral de reemplazo              
            x= c->Abrir(archivo, npalabras); //en el array x, se devolveran todas las palabras leidas en la clase carchivo. 
            string ch;    
            int repetidas[100];
            /**En este for se recorrerá tanto el texto como el array "x" donde se almacenan las palabras y así se obtendra un array de enteros con la cantidad de veces que se repite cada palabra**/
            for(int a=0; a<npalabras ;a++){
                int count=0;
                fstream texto2;
                texto2.open(archivo, ios::in); //abrir archivo en modo lectura 
                const char* data = x[a].data();   
                while(texto2)
                {
                    texto2  >> ch;
                    const char* recorrer = ch.data(); 
                    if(strcmp(recorrer,data)==0){
                        count++;
                    }
                    else{
                    }
                }
                 repetidas[a]=count;
            }       
    int contador = 0;
    int rem = 0;
    vector<string> re;
    for (size_t k = 0; k <npalabras; k++)
    {
        for (size_t j = k; j < npalabras; j++)
        {        
            if (x[k] == x[j])
            {
                contador++;
            }            
        }

        if (contador >= umbral)
        {
            bool flag2 = false;

            for (size_t j = 0; j < re.size(); j++)
            {
                if (re[j] == x[k])
                {
                    flag2 = true;
                    break;
                    
                }
                
            }
            if (!flag2)
            {
                rem++;
                re.push_back(x[k]);
            }
 
        }
        contador = 0;     
    }
    

    /**Ahora se abrirá el archivo en modo escritura para la solución**/
    ofstream archivo;
    archivo.open("prueba.rep",ios::out);
    bool flag = false;
    for (size_t k = 0; k < npalabras; k++)
    {
        for (size_t j = 0; j < re.size(); j++)
        {
            if (x[k] == re[j])
            {
                archivo << "@" << j+1 << " ";
                flag = true;
                
            }else
            {
                
                     for ( size_t z = 0; z < re.size(); z++)
                    {
                    if (x[k] == re[z])
                    {
                        flag = true;
                        
                    }
                    }
                    if (!flag)
                    {
                        archivo << x[k] << " ";
                        flag = true;
                    }                           
                
            }           
            
        }

        flag = false;
        
    }
     archivo.close();
       } 
}
