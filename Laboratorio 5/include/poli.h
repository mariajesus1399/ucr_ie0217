#pragma once
#include <iostream>

using namespace std;

class Poli{
    private:
     int coeficiente2;
     int coeficiente1;
     int coeficiente0;
     int coeficiente4;
     int coeficiente3;
     public:
    Poli();
    Poli(int coeficiente4, int coeficiente3, int coeficiente2, int coeficiente1, int coeficiente0);
    /**
    * @brief Este metodo suma dos polinomios
    * @details Con la ayuda de los coeficiente suma termino a termino
    */
    Poli operator + (Poli const  &obj);
    /**
    * @brief Este metodo resta dos polinomios
    * @details Con la ayuda de los coeficiente resta termino a termino
    */
    Poli operator - (Poli const  &obj);
    /**
    * @brief Este metodo multiplica dos polinomios
    * @details Con la ayuda de los coeficientes va multiplicando de forma distributiva
    */
    Poli operator * (Poli const  &obj);
    /**
    * @brief Este metodo resta divide dos polinomios, uno de grado dos y otro de primer grado
    * @details Con la ayuda de division sintetica se obtiene el cociente
    */    
    Poli operator / (Poli const  &obj);
    /**
    * @brief Este metodo multiplica permite imprimir en pantalla
    */
    friend ostream& operator<< (ostream &os, Poli &obj);
    friend istream& operator>> (istream &is, Poli &obj);

};