#pragma once

#include <iostream>

using namespace std;

class Fraccion{
    

    public:
    int numerador;
    int denominador;
    Fraccion();
    Fraccion(int numerador, int denominador);
    /**
    * @brief Este metodo suma dos fracciones
    * @details Con la ayuda del metodo de multiplicar en cruz y sumar numeradores se obtiene la suma
    */
    Fraccion operator + (Fraccion const  &obj);
    /**
    * @brief Este metodo resta dos fracciones
    * @details Con la ayuda del metodo de multiplicar en cruz y restar numeradores se obtiene la diferencia
    */
    Fraccion operator - (Fraccion const  &obj);
    /**
    * @brief Este metodo resta multiplica dos fracciones
    * @details Se multiplica numerador con numerador y denominador con denominador y se obtiene el producto
    */
    Fraccion operator * (Fraccion const  &obj);
    /**
    * @brief Este metodo resta divide dos fracciones
    * @details Se multiplica numerador con denominador y denominador con numerador y se obtiene el cociente
    */
    Fraccion operator / (Fraccion const  &obj);
    /**
    * @brief Este metodo multiplica permite imprimir en pantalla
    */
    //Fraccion operator=(const CLASE &rhs)
    friend ostream& operator<< (ostream &os, Fraccion  &obj);
};