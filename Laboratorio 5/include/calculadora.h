
#include <iostream>
#include "fracc.h"
#include "poli.h"

#include <iostream>

using namespace std;

template <typename Data>
class Calculadora
{


    public:
      /**
      * @brief Este metodo suma dos numeros de cualquier tipo
      */
      
      Data Add(Data &d1, const Data &d2)
      { 
       Data result;
       result= d1+d2;
       return result;
      }
      /**
      * @brief Este metodo resta dos numeros de cualquier tipo
      */
      Data Sub(Data &d1, const Data &d2){
       Data result;
       result= d1-d2;
       return result;
      }
      /**
      * @brief Este metodo multiplica dos numeros de cualquier tipo
      */

      Data Mul(Data &d1, const Data &d2){
        Data result;
       result= d1*d2;
       return result;
       }
      /**
      * @brief Este metodo divide dos numeros de cualquier tipo
      */

      Data Div(Data &d1, const Data &d2){
        Data result;
       result= d1/d2;
       return result;
      }
 
};

