#include <iostream>
#include "fracc.h"
#include "poli.h"
#include "calculadora.h"
using namespace std;
int main(){
    cout<<"Esto es un programa de prueba donde se implementan distintos tipos de numero por medio de plantillas"<<endl;
    cout<<"--------------------------------------------------" <<endl; 
    Calculadora <int>c1;
    int x=10;
    int y=5;
    cout<<"Tenemos dos numeros enteros"<<endl;
    cout<<"Primer numero: "<<x<<endl;
    cout<<"Segundo numero: "<<y<<endl;
    cout<<"La suma es "<<c1.Add(x,y)<<endl;
    cout<<"La resta es "<<c1.Sub(x,y)<<endl;
    cout<<"El producto es "<<c1.Mul(x,y)<<endl;
    cout<<"El cociente es "<<c1.Div(x,y)<<endl;

    cout<<"--------------------------------------------------" <<endl; 

    Calculadora <double>c4;
    double j=100.5;
    double k=90.3;
    cout<<"Tenemos dos numeros flotantes"<<endl;
    cout<<"Primer numero: "<<j<<endl;
    cout<<"Segundo numero: "<<k<<endl;
    cout<<"La suma es "<<c4.Add(j,k)<<endl;
    cout<<"La resta es "<<c4.Sub(j,k)<<endl;
    cout<<"El producto es "<<c4.Mul(j,k)<<endl;
    cout<<"El cociente es "<<c4.Div(j,k)<<endl;

    cout<<"--------------------------------------------------" <<endl; 
    cout<<"Fracciones"<<endl;

    Calculadora<Fraccion>c2;
    Fraccion f1(2,3);
    Fraccion f2(1,2);
    cout<<"Fraccion 1:"<<endl;
    cout<<f1<<endl;
    cout<<"Fraccion 2:"<<endl;
    cout<<f2<<endl;
    Fraccion f3= c2.Add(f1,f2);
    Fraccion f4 =c2.Sub(f1,f2);
    Fraccion f5 =c2.Mul(f1,f2);
    Fraccion f6 =c2.Div(f1,f2);
    cout<<"La suma es "<<f3<<endl;
    cout<<"La resta es "<<f4<<endl;
    cout<<"El producto es "<<f5<<endl;
    cout<<"El cociente es "<<f6<<endl;

    cout<<"--------------------------------------------------" <<endl; 
    cout<<"Polinomios"<<endl;

    Calculadora<Poli>c3;
    Poli p1(0,0,1,1,-20);
    Poli p2(0,0,0, 1,5);
    cout<<"Polinomio 1:"<<endl;
    cout<<p1<<endl;
    cout<<"Polinomio 2:"<<endl;
    cout<<p2<<endl;
    cout<<"Los dos polinomios anteriores se utilizaran para el metodo de division, por ser una division especial"<<endl;
    Poli pp=c3.Div(p1,p2);
    cout<<"Polinomio1/Polonomio2:"<<endl;
    cout<<pp<<endl;

    Poli p3(0,0,2,2,9);
    Poli p4(0,0,4,-3,1);
    cout<<"Polinomio 3:"<<endl;
    cout<<p3<<endl;
    cout<<"Polinomio 4:"<<endl;
    cout<<p4<<endl;   
    Poli p5= c3.Add(p3,p4);
    Poli p6 =c3.Sub(p3,p4);
    Poli p7 =c3.Mul(p3,p4);

    cout<<"La suma es "<<p5<<endl;
    cout<<"La resta es "<<p6<<endl;
    cout<<"El producto es "<<p7<<endl;


    


   






return 0;
}