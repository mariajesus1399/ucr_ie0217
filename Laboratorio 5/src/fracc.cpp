#include "fracc.h"

Fraccion::Fraccion(){
    this->numerador = 0;
    this->denominador=0;


}

Fraccion::Fraccion(int numerador, int denominador){
    this->numerador = numerador;
    this->denominador=denominador;
}

Fraccion Fraccion::operator + (Fraccion const &obj){
    Fraccion result;
    result.numerador= (this->numerador)*obj.denominador+this->denominador*obj.numerador;
    result.denominador =this->denominador * obj.denominador;
    return result;
}

Fraccion Fraccion::operator - (Fraccion  const &obj){
    Fraccion result;
    result.numerador= (this->numerador)*obj.denominador-this->denominador*obj.numerador;
    result.denominador =this->denominador * obj.denominador;
    return result;
}

Fraccion Fraccion::operator * (Fraccion  const &obj){
    Fraccion result;
    result.numerador= (this->numerador)*obj.numerador;
    result.denominador =this->denominador * obj.denominador;
    return result;
}

Fraccion Fraccion::operator / (Fraccion const   &obj){
    Fraccion result;
    result.numerador= (this->numerador)*obj.denominador;
    result.denominador =this->denominador * obj.numerador;
    return result;
}

ostream& operator << (ostream& os, Fraccion  &obj){
    os << obj.numerador << " / " << obj.denominador ;
    return os;
}