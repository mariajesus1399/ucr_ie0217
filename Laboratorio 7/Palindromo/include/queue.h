
#include<iostream>

using namespace std;

class Node
{
public:
    /**
     * @brief Creamos un puntero que puede ser utilizado en la clase queue
     */
    char letra;
    Node *next;
};

class Queue
{
    public:
    Node *head;
    Node *tail;
    Node *prev;
    Node *temp;
    bool isEmpty()
    {
        return head == NULL;
    }
    
    /**
     * @brief Constructor
     */
    Queue()
    {
        head = NULL;
        tail = NULL;
    }

     /**
     * @brief Constructor que recibe datos y forma el queue
     */
    void enqueue(char x)
    {
        temp = new Node;
        temp->letra = x;
        temp->next = NULL;
        if(isEmpty())
        {
            head = temp;
            tail = temp;
        }
        else
        {
            prev = tail;
            tail->next = temp;
            tail = temp;
        }
    }


     /**
     * @brief Funcion que elimina al frente
     */
    
    void popfront()
    {
        temp = head;
        head = head->next;
        delete temp;
    }
     /**
     * @brief Funcion que elimina el ultimo elemento en la lista
     */
    void popback()
    {  
      
     Node *temp = head;
     while (temp->next->next != NULL) {
     temp = temp->next;
     }
     tail = temp;
     delete tail->next;
     tail->next = NULL;
    }


};
