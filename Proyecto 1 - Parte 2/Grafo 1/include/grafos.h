#pragma once 
#include <iostream>
#include<ctime>
#include <lemon/list_graph.h>
#include <queue>
#include <stack>


using namespace std;
using namespace lemon;

/*
@brief Profundidad es el metodo utilizado para recorrer el grafo de esa manera
@details El stack es de suma utilidad para poder implementar el recorrido 
*/

void Profundidad(ListDigraph &grafo, ListDigraph::Node dNode){
    stack<ListDigraph::Node> pila;
  	int numerovertices = countNodes(grafo); //countNodes es una funcion que brinda la libreria
	bool visitado[numerovertices];
	for (int i = 0; i < numerovertices; i++)
	{
		visitado[i] = false;
	}

    pila.push(dNode); //ingresar el nodo en la cola
    
    while(!pila.empty()){
        ListDigraph::Node aNode = pila.top();
        pila.pop();

        if(!visitado[grafo.id(aNode)])
			{
				visitado[grafo.id(aNode)] = true;
                cout << grafo.id(aNode) << " -> ";
			}

            for (ListDigraph::OutArcIt punteroa(grafo,aNode); punteroa != INVALID; ++punteroa){
            ListDigraph::Node nNode = grafo.target(punteroa); //busca el siguiente
            if(!visitado[grafo.id(nNode)]){
			pila.push(nNode);  //ingresa a la pila
			}
        
        }

       

    }
	cout<<"Final"<<endl;

    



}



/*
@brief Ancho es el metodo utilizado para recorrer el grafo de esa manera
@details La cola es de suma utilidad para poder implementar el recorrido 
*/
void Ancho(ListDigraph &grafo, ListDigraph::Node dNode){
    queue<ListDigraph::Node> cola;
	int numerovertices = countNodes(grafo); //countNodes es una funcion que brinda la libreria
	bool visitado[numerovertices];
	for (int i = 0; i < numerovertices; i++)
	{
		visitado[i] = false;
	}
	visitado[grafo.id(dNode)] = true; //id es un atributo de la clase lemon
    cola.push(dNode); //ingresar el nodo en la cola
	while (!cola.empty())   //hasta que la cola este vacia
	{
		ListDigraph::Node aNode = cola.front(); //mi primer valor de la cola
		cout << grafo.id(aNode) << " -> ";
		cola.pop();
		for (ListDigraph::OutArcIt punteroa(grafo,aNode); punteroa != INVALID; ++punteroa) // OutArIt indica cuales aristas salen del nodo
		{
			ListDigraph::Node nNode = grafo.target(punteroa); //busca el siguiente
			if(!visitado[grafo.id(nNode)])
			{
				visitado[grafo.id(nNode)] = true;
				cola.push(nNode);  //ingresa en la cola
			}
		}
		
	}
	cout<<"Final"<<endl;

}

/*
@brief Prim es el metodo utilizado para buscar el camino con menos peso de un grafo, puede ser que no sea el mas optimo
@details La pila es de suma utilidad para poder implementar este metodo 
*/

void Prim(ListDigraph &grafo, ListDigraph::Node dNode, ListDigraph::ArcMap<int> &costs){

   
   int numerovertices = countNodes(grafo);
   stack<ListDigraph::Node> pila;
    bool visitado[numerovertices];
   	for (int i = 0; i < numerovertices; i++)
	{
		visitado[i] = false;
	}

   visitado[grafo.id(dNode)]=true;
   pila.push(dNode);

   while(!pila.empty()){
	    ListDigraph::Node aNode = pila.top(); //mi primer valor de la cola
		cout << grafo.id(aNode) << " -> ";
		pila.pop();
		int valormaximo=100;

	  for (ListDigraph::OutArcIt punteroa(grafo,aNode); punteroa != INVALID; ++punteroa){
          ListDigraph::Node nNode = grafo.target(punteroa); //busca el siguiente
			if(!visitado[grafo.id(nNode)])
			{
				if(costs[punteroa]<valormaximo){
					valormaximo=costs[punteroa];
					visitado[grafo.id(nNode)] = true;
					pila.push(nNode);  //ingresa en la cola

				}
			}
	}
}
cout<<"Final"<<endl;




}



