#include <iostream>
#include "grafos.h"
#include <iostream>
#include <vector>

// LEMON includes


using namespace lemon;
using namespace std;

int main(){
 	ListDigraph gr;
  	
 // Bi-Directional graph

    
    // Graph nodes
    ListDigraph::Node a = gr.addNode();
    ListDigraph::Node b = gr.addNode();
    ListDigraph::Node c = gr.addNode();
    ListDigraph::Node d = gr.addNode();
    ListDigraph::Node e = gr.addNode();
    ListDigraph::Node f = gr.addNode();
    ListDigraph::Node g = gr.addNode();
    ListDigraph:: Node h= gr.addNode();
    ListDigraph:: Node i= gr.addNode();


    // Graph edges
    ListDigraph::Arc ab = gr.addArc(a, b);
    ListDigraph::Arc ah = gr.addArc(a, h);
    ListDigraph::Arc bc = gr.addArc(b, c);
    ListDigraph::Arc bh = gr.addArc(b, h);
    ListDigraph::Arc cd = gr.addArc(c, d);
    ListDigraph::Arc cf = gr.addArc(c, f);
    ListDigraph::Arc ci = gr.addArc(c, i);
    ListDigraph::Arc de = gr.addArc(d, e);
    ListDigraph::Arc df = gr.addArc(d, f);
    ListDigraph::Arc ef = gr.addArc(e, f);
    ListDigraph::Arc fg = gr.addArc(f, g);
    ListDigraph::Arc gh = gr.addArc(g, h);
    ListDigraph::Arc gi = gr.addArc(g, i);
    ListDigraph::Arc hi = gr.addArc(h, i);
    
    ListDigraph::Arc ba = gr.addArc(b, a);
    ListDigraph::Arc ha = gr.addArc(h, a);
    ListDigraph::Arc cb = gr.addArc(c, b);
    ListDigraph::Arc hb = gr.addArc(h, b);
    ListDigraph::Arc dc = gr.addArc(d, c);
    ListDigraph::Arc fc = gr.addArc(f, c);
    ListDigraph::Arc ic = gr.addArc(i, c);
    ListDigraph::Arc ed = gr.addArc(e, d);
    ListDigraph::Arc fd = gr.addArc(f, d);
    ListDigraph::Arc fe = gr.addArc(f, e);
    ListDigraph::Arc gf = gr.addArc(g, f);
    ListDigraph::Arc hg = gr.addArc(h, g);
    ListDigraph::Arc ig = gr.addArc(i, g);
    ListDigraph::Arc ih = gr.addArc(i, h);
   

      

    // Edges costs
    ListDigraph::ArcMap<int> costs(gr);
    costs[ab] = 4;
    costs[ah] = 8;
    costs[bc] = 8;
    costs[bh]=11;
    costs[cd]=7;
    costs[cf]=4;
    costs[ci]=2; 
    costs[de]=9;
    costs[df]=14;
    costs[ef]=10;
    costs[fg]=2;
    costs[gh]=1;
    costs[gi]=6;
     costs[hi]=7;

    costs[ba] = 4;
    costs[ha] = 8;
    costs[cb] = 8;
    costs[hb]=11;
    costs[dc]=7;
    costs[fc]=4;
    costs[ic]=2; 
    costs[ed]=9;
    costs[fd]=14;
    costs[fe]=10;
    costs[gf]=2;
    costs[hg]=1;
    costs[ig]=6;
    costs[ih]=7;


    ///////////////////////////

   
    
  
  cout <<"Grafo 1"<<endl;
  cout << endl << "Ancho: " << endl;
	Ancho(gr,a);
  cout << endl << "--------------------------------- " << endl;
  
	cout << endl << "Profundidad: " << endl;
	Profundidad(gr,a); 
	cout << endl << "--------------------------------- " << endl;
	

    
	cout << endl << "Prim: " << endl;
  Prim (gr,a,costs);
  cout << endl << "--------------------------------- " << endl;


 



	return 0;
}

