#include <iostream>
#include "grafos.h"
#include <iostream>
#include <vector>

// LEMON includes


using namespace lemon;
using namespace std;

int main(){
 	ListDigraph gr2;
  	
 // Bi-Directional graph

    
   

  	
      // Graph nodes
    ListDigraph::Node a = gr2.addNode();
    ListDigraph::Node b = gr2.addNode();
    ListDigraph::Node c = gr2.addNode();
    ListDigraph::Node d = gr2.addNode();
    ListDigraph::Node e = gr2.addNode();
    ListDigraph::Node f = gr2.addNode();
    ListDigraph::Node g = gr2.addNode();
    ListDigraph:: Node h= gr2.addNode();
    ListDigraph:: Node i= gr2.addNode();
    ListDigraph:: Node j= gr2.addNode();

    // Graph edges
    ListDigraph::Arc af = gr2.addArc(a, f);
    ListDigraph::Arc ah = gr2.addArc(a, h);
    ListDigraph::Arc ai = gr2.addArc(a, i);
    ListDigraph::Arc bf = gr2.addArc(b, f);
    ListDigraph::Arc bg = gr2.addArc(b, g);
    ListDigraph::Arc bh = gr2.addArc(b, h);
    ListDigraph::Arc bj = gr2.addArc(b, j);
    ListDigraph::Arc cf = gr2.addArc(c, f);
    ListDigraph::Arc ce = gr2.addArc(c, e);
    ListDigraph::Arc dg = gr2.addArc(d, g);
    ListDigraph::Arc dj = gr2.addArc(d, j);
    ListDigraph::Arc ef = gr2.addArc(e, f);
    ListDigraph::Arc ej = gr2.addArc(e, j);
    ListDigraph::Arc gh = gr2.addArc(g, h);
    
    ListDigraph::Arc fa = gr2.addArc(f, a);
    ListDigraph::Arc ha = gr2.addArc(h, a);
    ListDigraph::Arc ia = gr2.addArc(i, a);
    ListDigraph::Arc fb = gr2.addArc(f, b);
    ListDigraph::Arc gb = gr2.addArc(g, b);
    ListDigraph::Arc hb = gr2.addArc(h, b);
    ListDigraph::Arc jb = gr2.addArc(j, b);
    ListDigraph::Arc fc = gr2.addArc(f, c);
    ListDigraph::Arc ec = gr2.addArc(e, c);
    ListDigraph::Arc gd = gr2.addArc(g, d);
    ListDigraph::Arc jd = gr2.addArc(j, d);
    ListDigraph::Arc fe = gr2.addArc(f, e);
    ListDigraph::Arc je = gr2.addArc(j, e);
    ListDigraph::Arc hg = gr2.addArc(h, g);

      

    // Edges costs
    ListDigraph::ArcMap<int> costs(gr2);
    costs[af] = 343;
    costs[ah] = 1435;
    costs[ai] = 464;
    costs[bf] = 879;
    costs[bg] = 954;
    costs[bh] = 811;
    costs[bj] = 524;
    costs[cf] = 1054;
    costs[ce] = 1364;
    costs[dg] = 433;
    costs[dj] = 1053;
    costs[ef] = 1106;
    costs[ej] = 766;
    costs[gh] = 837;

    costs[fa] = 343;
    costs[ha] = 1435;
    costs[ia] = 464;
    costs[fb] = 879;
    costs[gb] = 954;
    costs[hb] = 811;
    costs[jb] = 524;
    costs[fc] = 1054;
    costs[ec] = 1364;
    costs[gd] = 433;
    costs[jd] = 1053;
    costs[fe] = 1106;
    costs[je] = 766;
    costs[hg] = 837;
    

  cout <<"Grafo 2"<<endl;
  cout << endl << "Ancho: " << endl;
	Ancho(gr2,a);
  cout << endl << "--------------------------------- " << endl;
  
	cout << endl << "Profundidad: " << endl;
	Profundidad(gr2,a); 
	cout << endl << "--------------------------------- " << endl;
	

    //sirve con d, e, g, i
	cout << endl << "Prim: " << endl;
  Prim (gr2,i,costs);
  cout << endl << "--------------------------------- " << endl;





	return 0;
}

