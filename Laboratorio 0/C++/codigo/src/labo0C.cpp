/**
	* @file labo0C.cpp
	* @version 1.0
	* @date 07/01/2020
	* @author Aguilar, Maria Jesus. Alfano, Angelica. Cerdas, Katherine
*/

/**RESUMEN
    El siguiente programa permite obtener una cadena de aminoácidos si el usuario
    ingresa una cadena de codones de forma correcta, deben ir en grupos de 3, por
    lo que el largo de la cadena debe ser múltiplo de 3, no se consideraron excepciones, 
    además de que no hay funciones ni clases 
*/
#include <stdio.h>
#include <cstring>
#include <iostream>

using namespace std;
 
int main (int argc, char *argv[]) {/**< argc contiene el numero de argumentos que se han introducido */


int contador2=0;
int contador1=contador2*3;
int x= contador1+0; /**< Posicion x a recorrer */
int y= contador1+1; /**< Posicion y a recorrer */
int z= contador1+2; /**< Posicion z a recorrer*/
string resultado[500]; /**< Array de strings */
std::string stri= argv[1];

int limite = stri.length()/3;  

char cadena[stri.length()];
strcpy(cadena, argv[1]); 
while (contador2 < limite) {
if (cadena[x]=='U') {
    if (cadena[y]=='U') {

        if (cadena[z]=='U' ||cadena[z]=='C'){
            resultado[contador2]='F';
        } else {
            resultado[contador2]='L';
        }
    } else if (cadena[y]=='C'){
        resultado[contador2]='S';

    } else if (cadena[y]=='A'){
        if (cadena[z]=='U' ||cadena[z]=='C'){
          resultado[contador2]='Y';  
        } else {
            cadena[z]=='0';
        }
    } else {
        if (cadena[z]=='U' ||cadena[z]=='C') {
            resultado[contador2]='C';  
        } else if (cadena[z]=='G') {
            resultado[contador2]='W'; 
        } else {
            cadena[z]=='0';
        }
    } 
} else if (cadena[x]=='G') {
    if (cadena[y]=='U'){
        resultado[contador2]='V';  
    } else if (cadena[y]=='C') {
        resultado[contador2]='A'; 
    } else if (cadena[y]=='A') {
        if (cadena[z]=='U' ||cadena[z]=='C'){
            resultado[contador2]='D'; 
        } else if (cadena[z]=='A' ||cadena[z]=='G') {
            resultado[contador2]='E'; 
        
    } else if (cadena[y]=='G'){
        resultado[contador2]='G'; 
    }
}

}
else if (cadena[x]=='A'){

    if (cadena[y]=='U'){
        if (cadena[z]=='G'){
            resultado[contador2]='M'; 
        }
        else {
            resultado[contador2]='I'; 
        }
    }
    else if(cadena[y]=='C'){
        resultado[contador2]='T'; 
    }
    else if (cadena[y]=='A'){
        if (cadena[z]=='G'|| cadena[z]=='A'){
            resultado[contador2]='K'; 
        }
        else {
            resultado[contador2]='N'; 
        }
    } else {
        if (cadena[z]=='G'|| cadena[z]=='A'){
           resultado[contador2]='R';  
        }
        else{
            resultado[contador2]='S'; 
        }
    }
}
else {
    if (cadena[y]=='U'){
        resultado[contador2]='L'; 
    }
    else if (cadena[y]=='C'){
        resultado[contador2]='P'; 
    }
    else if (cadena[y]=='G'){
        resultado[contador2]='R'; 
    }
    else if (cadena[z]=='U'||cadena[z]=='C'){
        resultado[contador2]='H'; 
    }
    else {
        resultado[contador2]='Q'; 
    }
}
cout<< resultado[contador2] ;
contador2=contador2 +1;
contador1=contador2*3;
x= contador1+0;
y= contador1+1;
z= contador1+2;

}
}
