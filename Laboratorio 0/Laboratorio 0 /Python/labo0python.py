import sys
import math

def main():
#A Continuacion el docstring de la funcion
  """Resumen

     El siguiente programa permite obtener una cadena de aminoacidos
     si el usuario ingresa una cadena de codones de forma correcta
     deben ir en grupos de tres, por lo que debe ser multiplo de tres 
     el largo de la cadena, es importante mencionar esto ya que no se tomaron 
     excepciones, ademas de que no hay funciones, solo el main. Por lo que
     este es el unico docstring, y por ultimo no fue necesario utilizar
     clases"""

  cadena = str(sys.argv[1]) #se obtiene el parametro del usuario
  largo= len(cadena) #largo de la cadena obtenida

  contador2=0  #inicializamos valores
  contador1= contador2*3
  x=contador1 + 0 #Primera posicion del tercio
  y= contador1+ 1 #Segunda posicion del tercio
  z= contador1 +2 #Tercera posicion del tercio
  resultado=[]


  limite=largo/3  #calculamos el limite

  while contador2 < limite: 
    #todos las condiciones que se necesitan para poder obtener
    #la secuencia de aminoacidos

      if cadena[x]=="U":
        if cadena[y]== 'U':
          if (cadena[z]== 'U' or cadena[z]== 'C'):
            resultado.append('F') #todos los append guardan en el array los valores

          
          else:
            resultado.append('L')


        elif cadena[y]=='C':
            resultado.append('S')

        elif cadena[y]=='A':
            if (cadena[z]== 'U' or cadena[z]== 'C'):
                resultado.append('Y')
          
            else :
              resultado.append('|')


        else:
            if (cadena[z]== 'U' or cadena[z]== 'C'):
              resultado.append('C')
            elif cadena[z]=='G':
              resultado.append('W')
            else:
                resultado.append('|')


      elif cadena[x]=='G':
          if cadena[y]=='U':
              resultado.append("V")
          elif cadena[y]=='C':
              resultado.append('A')
          elif cadena[y]=='A':
              if (cadena[z]=='U' or cadena[z]=='C'):
                  resultado.append("D")
              elif (cadena[z]=='A' or cadena[z]=='G'):
                  resultado.append('E')
          elif cadena[y]=='G':
              resultado.append('G')

      elif cadena[x]=='C':
          if cadena[y]=='U':
            resultado.append('L')
          elif cadena[y]=='C':
            resultado.append("P")
          elif cadena[y]=='A':
            if (cadena[z]=='U' or cadena[z]=='C'):
                  resultado.append("H")
            else:
                  resultado.append('Q')
          elif cadena[y]=='G':
            resultado.append("R")
      elif cadena[x]=='A':
        if cadena[y]== 'U':
          if (cadena[z]== 'G' ):
            resultado.append('M')

          
          else:
            resultado.append('I')

        elif cadena[y]=='C':
            resultado.append('T')

        elif cadena[y]=='A':
            if (cadena[z]== 'U' or cadena[z]== 'C'):
                resultado.append('N')
          
            else:
              resultado.append('K')


        else:
            if (cadena[z]== 'U' or cadena[z]== 'C'):
              resultado.append('S')
          
            else:
                resultado.append('R')
      
      
      #los valores iran cambiando
      #x,y y z corresponden a cada posicion de la triada que
      #esta siendo evaluada
      contador2=contador2+1
      contador1= contador2*3
      x=contador1 + 0
      y= contador1+ 1
      z= contador1 +2
  #pasamos  resultado que es un array a un string
  string= "".join(resultado)
  print (main.__doc__) #se encarga de imprimir el docstring
  print ("La cadena de aminoacidos es:")
  print(string)

main()