#include <string>
#include <iostream>

#include "pokemon.h"
#include "articuno.h"
#include "zapdos.h"
#include "moltres.h"
#include "flying.h"
#include "ice.h"
#include "fire.h"

using namespace std;

int main(){
  /*Creación de los pokemones*/
  Pokemon *pokemon;
  Articuno articuno;
  pokemon = &articuno;

  Pokemon *po;
  Zapdos zapdos;
  po=&zapdos;

  Pokemon *p;
  Moltres moltres;
  p=&moltres;

  /*Imprimir información inicial*/
  string fortalezamoltres;
  string tipoarticuno;
  articuno.printInfo();
  moltres.printInfo();
  zapdos.printInfo();

  cout<<"----------------------------------------------------------------------------"<<endl;
  //Primera Batalla Moltres vs Articuno


  cout<<"Comienza la Batalla de la Frontera entre Noland, el cerebro de la fábrica de la batalla y su retador Ash del Pueblo Paleta "<<endl;
  cout<<"*El techo se abre, esto para que los pokemones voladores puedan usar su potencial*"<<endl;
  cout<<"Moltres aparece"<<endl;
  moltres.callf();
  cout<<"Articuno aparece"<<endl;
  articuno.callf();
  cout<<"¡Entrenadores, ahora!"<<endl;
  fortalezamoltres=moltres.Fire::strongVs();
  tipoarticuno=articuno.Ice::type();
  cout<< "Moltres ataca primero"<<endl;
  moltres.atk1(*pokemon);
  moltres.callf();
  if (fortalezamoltres==tipoarticuno){
    cout<< "Articuno es debil contra Moltres, entonces pierde mas vida"<<endl;
    articuno.HP=articuno.HP-100;
  }
  else {
  }
    //cout<<"Articuno se defiende"<<endl;
    articuno.atk3(*p);
    articuno.callf();


  cout<<"Moltres utiliza ataque especial"<<endl;
  moltres.atk2(*pokemon);
  moltres.callf();
  cout<<"Articuno sale de la batalla"<<endl;
  cout<<"----------------------------------------------------------------------------"<<endl;

  articuno.printInfo();
  moltres.printInfo();
  zapdos.printInfo();

  cout<<"----------------------------------------------------------------------------"<<endl;
  string fortalezazapdos;
  string tipomoltres;
  //Segunda Batalla Moltres vs Zapdos
  cout<<"Empieza la segunda Batalla de la Frontera"<<endl;
  cout<<"Zapdos aparece"<<endl;
  zapdos.callf();
  cout <<"Moltres continua en el campo de batalla"<<endl;
  moltres.callf();
  cout<< "Zapdos ataca primero"<<endl;
  fortalezazapdos=zapdos.Electric::strongVs();
  tipomoltres=moltres.Flying::type();
  zapdos.atk1(*p);
  zapdos.callf();
  if (fortalezazapdos==tipomoltres){
    cout<< "Moltres es débil contra Zapdos, entonces pierde más vida"<<endl;
    moltres.HP=moltres.HP-100;
  }
  else {
  }
    cout<<"Moltres utiliza defensa especial"<<endl;
    moltres.atk4(*po);
    moltres.callf();
    zapdos.atk2(*p);
    zapdos.callf();
    cout<<"Moltres sale de la batalla"<<endl;
    cout<<"----------------------------------------------------------------------------"<<endl;
}
