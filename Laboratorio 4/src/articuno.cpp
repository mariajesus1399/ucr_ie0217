#include "articuno.h"
#include <iostream>
#include <string>

Articuno::Articuno(){
    this->name="Articuno";
    this->species="Congelar";
    this->Type="Hielo";
    this->strongVs="Tierra" ;
    this->weakVs="Fuego";
    this->ATK =85;
    this->HP = 500;
    this->DEF =100 ;
    this->sATK =95 ;
    this->sDEF =125;
    this->SPD =85;
    this->EXP=21;
    this->call="wuaaaaaaaa";

}

void Articuno::atk1(Pokemon&other){
    cout<<"Articuno acata con vaho gélido a " <<other.name<<endl;
    other.HP=other.HP-85;
}
void Articuno::atk2(Pokemon&other){
    cout<<"Articuno acata con rayo de hielo a " <<other.name<<endl;
    other.HP=other.HP-95;
}
void Articuno::atk3(Pokemon&other){
    cout<<"Articuno se defiende con canto helado contra " <<other.name<<endl;
    other.HP=other.HP-100;
}
 void Articuno::atk4(Pokemon&other){
    cout<<"Articuno se defiende con rayo de hielo contra " <<other.name<<endl;
    other.HP=other.HP-125;
}



Articuno::~Articuno(){

}
