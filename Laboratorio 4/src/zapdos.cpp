#include "zapdos.h"
#include <string>
#include <iostream>

Zapdos::Zapdos(){
    this->name="Zapdos";
    this->species="Electrico";
    this-> Type="Electrico";
    this->strongVs="Volador" ;
    this->weakVs="Hierba";
    this->ATK =90 ;
    this->HP = 500;
    this->DEF =85 ;
    this->sATK =125 ;
    this->sDEF =90;
    this->SPD =100;
    this->EXP=261;
    this->call="zaaaaaaaa";
}

void Zapdos::atk1(Pokemon&other){
    cout<<"Zapdos acata con impactrueno a " <<other.name<<endl;
    other.HP=other.HP-90;
}
void Zapdos::atk2(Pokemon&other){
    cout<<"Zapdos acata con rayo a " <<other.name<<endl;
    other.HP=other.HP-125;
}
void Zapdos::atk3(Pokemon&other){
    cout<<"Zapdos se defiende con rayo de carga contra  " <<other.name<<endl;
    other.HP=other.HP-85;;
}
void Zapdos::atk4(Pokemon&other){
    cout<<"Zapdos se defiende con electrocañon contra " <<other.name<<endl;
    other.HP=other.HP-90;
}

Zapdos::~Zapdos(){

}
