#pragma once

#include <string>

#include "pokemon.h"

using namespace std;

class Flying : virtual public Pokemon{
    public:
        /**Default constructor. */
        Flying();
        /** Get type
        @return type
        */
        static string type();
        /** Get the strongVs
        @return strongVs
        */
        static string strongVs();
        /** Get the weakVs
        @return weakVs
        */
        static string weakVs();
         /**Default destructor */
        ~Flying();
};
