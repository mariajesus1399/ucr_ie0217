#pragma once

#include <string>

#include "pokemon.h"

using namespace std;

class Ice : virtual public Pokemon{
    public:
      /**Default constructor. */
        Ice();
        /** Get type
        @return type
        */
        static string type();
        /** Get the strongVs
        @return strongVs
        */
        static string strongVs();
        /** Get the weakVs
        @return weakVs
        */
        static string weakVs();
         /**Default destructor */
        ~Ice();
};
