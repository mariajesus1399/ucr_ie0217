#pragma once

#include <string>

using namespace std;

class Pokemon{
    public:
       /**Default constructor. */
        Pokemon();
        /** Get attack
        @return
        */
        virtual void atk1(Pokemon&other) = 0; /* Método virtual puro o abstracto */
        /** Get attack
        @return
        */
        virtual void atk2(Pokemon&other) = 0; /* Método virtual puro o abstracto */
        /** Get attack
        @return
        */
        virtual void atk3(Pokemon&other) = 0; /* Método virtual puro o abstracto */
        /** Get attack
        @return
        */
        virtual void atk4(Pokemon&other) = 0; /* Método virtual puro o abstracto */
        /** Get sound of the pokemon
        @return sound
        */
        void callf();
        /** print information
        @return
        */
        void printInfo();
        string name;
        string species;
        int HP; 
        int ATK;
        int DEF;
        int sATK;
        int sDEF;
        int SPD;
        int EXP;
        string call;
         /**Default destructor */
        ~Pokemon();

};
