#pragma once

#include <string>
#include "electric.h"
#include "fire.h"
#include "ice.h"
#include "flying.h"
using namespace std;

class Moltres : public Fire, public Flying{
    public:
      /**Default constructor. */
        Moltres();
        string Type;
        string strongVs;
        string weakVs;
        /** Get information
        @return
        */
        void print();
         /**Default destructor */
          ~Moltres();
          /** Get attack
          @return
          */
        void atk1(Pokemon&other);
        /** Get attack
        @return
        */
        void atk2(Pokemon&other);
        /** Get attack
        @return
        */
        void atk3(Pokemon&other);
        /** Get attack
        @return
        */
        void atk4(Pokemon&other);


};
