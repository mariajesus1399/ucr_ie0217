#pragma once

#include <string>
#include "electric.h"
#include "fire.h"
#include "ice.h"
#include "flying.h"
using namespace std;

class Articuno : public Ice,  public Flying{
    public:
        /**Default constructor. */
        Articuno();
        string Type;
        string strongVs;
        string weakVs;
        /** Print information
        @return
        */
        void print();
         /**Default destructor */
        ~Articuno();
        /** Get attack
        @return
        */
        void atk1(Pokemon&other);
        /** Get attack
        @return
        */
        void atk2(Pokemon&other);
        /** Get attack
        @return
        */
        void atk3(Pokemon&other);
        /** Get attack
        @return
        */
        void atk4(Pokemon&other);
};
