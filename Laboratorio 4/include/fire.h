#pragma once

#include <string>

#include "pokemon.h"

using namespace std;

class Fire : virtual public Pokemon{
    public:
       /**Default constructor. */
        Fire();
        /** Get type
        @return type
        */
        static string type();
        /** Get the strongVs
        @return strongVs
        */
        static string strongVs();
        /** Get the weakVs
        @return weakVs
        */
        static string weakVs();
         /**Default destructor */
        ~Fire();
};
