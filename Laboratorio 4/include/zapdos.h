#pragma once

#include <string>
#include "pokemon.h"
#include "electric.h"
#include "fire.h"
#include "ice.h"
#include "flying.h"
using namespace std;

class Zapdos :public Electric, public Flying{
    public:
        /**Default constructor. */
        Zapdos();
        string Type;
        string strongVs;
        string weakVs;
        /** Get information
        @return
        */
        void print();
        /** Get attack
        @return
        */
        void atk1(Pokemon&other);
        /** Get attack
        @return
        */
        void atk2(Pokemon&other);
        /** Get attack
        @return
        */
        void atk3(Pokemon&other);
        /** Get attack
        @return
        */
        void atk4(Pokemon&other);
         /**Default destructor */
        ~Zapdos();

};
