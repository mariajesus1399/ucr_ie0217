#include "Tablero.h"
Tablero::Tablero(int){
this->jugadorarray= new Jugador[4];

this->tablero[0] = new Casilla("Inicio", "Inicio", 0, 0); 
this->tablero[1] = new Casilla("Parque Diversiones","Propiedad",40,10);
this->tablero[2] = new Casilla("Parque La Sabana","Propiedad",60,10);
this->tablero[3] = new Casilla("Casilla de impuestos","Casilla de impuestos",0,0);
this->tablero[4] = new Casilla("Aeoropuerto Juan Santa Maria","Propiedad",200,10);
this->tablero[5] = new Casilla("Teatro Nacional","Propiedad",100,10);
this->tablero[6] = new Casilla("Museo de lo ninos","Propiedad",100,10);
this->tablero[7] = new Casilla("Basilica de los Angeles","Propiedad",100,10);
this->tablero[8] = new Casilla("Carcel","Carcel", 0,0);
this->tablero[9] = new Casilla("Tamarindo","Propiedad",140,10);
this->tablero[10] = new Casilla("Puerto Viejo","Propiedad",160,10);
this->tablero[11] = new Casilla ("Jaco","Propiedad",160,10);
this->tablero[12] = new Casilla("Tren al Atlantico","Propiedad",200,10);
this->tablero[13] = new Casilla("Cerro Chirripo","Propiedad",180, 10);
this->tablero[14] = new Casilla("Corcovado","Propiedad",200,10);
this->tablero[15] = new Casilla("Braulio Carrillo","Propiedad",220,10);
this->tablero[16] = new Casilla("Grecia","Propiedad",240,10);
this->tablero[17] = new Casilla("Conchal","Propiedad",240,10);
this->tablero[18] = new Casilla("ICE","Propiedad",150,10);
this->tablero[19] = new Casilla("Playa Danta","Propiedad",240,10);
this->tablero[20] = new Casilla("Playa Bonita","Propiedad",280,10);
this->tablero[21] = new Casilla("Aeropuerto Daniel Oduber","Propiedad",200,10);
this->tablero[22] = new Casilla("V. Irazu","Propiedad",300,10);
this->tablero[23] = new Casilla("V. Poas","Propiedad",320,10);
this->tablero[24] = new Casilla("AyA","Propiedad",150,10);
this->tablero[25] = new Casilla("V.Arenal","Propiedad",340,10);
this->tablero[26] = new Casilla("Carcel","Carcel", 0,0);
this->tablero[27] = new Casilla("Manuel Antonio","Propiedad",340,10);
this->tablero[28] = new Casilla ("Tortuguero","Propiedad",360,10);
this->tablero[29] = new Casilla ("Monteverde","Propiedad",360,10);
this->tablero[30] = new Casilla("Tren al pacifico", "Propiedad",200,10);
this->tablero[31] = new Casilla("Rio Celeste","Propiedad",360,10);
this->tablero[32] = new Casilla("Casilla de impuestos","Casilla de impuestos",0,0);
//this->tablero[32] = new Casilla("Isla del Coco","Propiedad",400,10);
this->tablero[33] = new Casilla("Cabo Blanco","Propiedad",400,10);
this->tablero[34] = new Casilla("Marino Ballena","Propiedad",420,10);
this->tablero[35] = new Casilla("Morera Soto","Propiedad",420,10);


}

Tablero ::~Tablero (){

}


void Tablero::PasarInicio(Jugador* jugadoractual){
  cout << "Esta pasando por el incio, se le entregaran 200"<< "\n";
  jugadoractual->plata += 200;

}
void Tablero::ComprarPropiedad(Jugador* jugadoractual, Casilla* casiactual){
    bool letra_valida;
	string compra;
		if (casiactual -> estadocompra == false){
			if(jugadoractual-> plata > casiactual -> precio){
                while(letra_valida==false){
				cout << "Desea comprar la propiedad " << casiactual -> nombre << " por " << casiactual -> precio << "? (s o n)" << endl;
				cin >> compra;
				if(compra == "s"){
					casiactual -> dueno = jugadoractual;
					jugadoractual -> plata -= casiactual -> precio;
			        letra_valida=true;
					casiactual -> estadocompra = true;
				}
                else if(compra == "n"){
                casiactual -> estadocompra = false;
                letra_valida=true;
                }
                else {
                    cout << "Ingrese una letra valida por favor" << endl;
                    letra_valida=false;
                }
            }
            }
                
			
            else{
                cout <<"No tiene el dinero suficiente para comprar esta propiedad" << endl;
            }
            }
    
           
		
		else{
			if(casiactual -> dueno != jugadoractual){
				jugadoractual -> plata -= casiactual -> renta;
				casiactual -> dueno -> plata += casiactual->renta;
				cout << "El propietario " << casiactual -> dueno -> nombre_jugador << " le cobra el valor de " << casiactual -> renta << " colones a " << jugadoractual -> nombre_jugador << endl;
			}
            else {
            cout <<"La propiedad no tiene dueno, siga avanzando" << endl;
            }
		}
}

    

void Tablero::impcobrar(Jugador* jugadoractual){
       int imp;
       cout<<"Desea 1.Pagar 200 o 2.Pagar el 10 porciento de su dinero"<< "\n";
       cin>>imp;
       if(imp==1){
             jugadoractual -> plata -= 200;

       }
       else{
             jugadoractual -> plata -= ((jugadoractual -> plata)*0.1);
     }
}


void Tablero::irCarcel(Jugador* jugadoractual){
  jugadoractual ->plata -= 100;
  cout<<"Estas en la carcel, vas a seguir jugando pero te rebajamos 100"<< "\n";
}

  

int Tablero::Mover(Jugador* jugadoractual){
    int pos = jugadoractual->moverse();
    if (pos>35){
        pos= pos-36;
        PasarInicio(jugadoractual); 
    }
    else {}
    cout << "El jugador " <<jugadoractual->nombre_jugador << " se mueve a la posicion:" << pos << endl;
    jugadoractual->posicion = pos; 
    return pos;
}




