#include "Jugador.h"
#include <cstdlib>
#include <ctime>

Jugador::Jugador(){
  this -> nombre_jugador = "";
 	this -> plata = 700; 
  this -> posicion = 0;
}

Jugador::Jugador(string nombre_jugador){
  this -> nombre_jugador = nombre_jugador;
 	this -> plata = 700; 
  this -> posicion = 0;

}

 Jugador::~Jugador(){
 }

 int Jugador::tirar_dado(){
    int ran;
    srand (time (0)); //Inicializar numeros aleatorios
    ran = rand () % 6 + 1; //Se guarda en ran (que es un entero) un numero aleatorio entre 1 y 6. 
    return ran;
 }

int Jugador::moverse(){

  cout<<"Los dos dados se tiran automaticamente" << "\n";
  int dado1 = tirar_dado();

  int dado2 = tirar_dado();
  
  return this->posicion+dado1+dado2;
}

