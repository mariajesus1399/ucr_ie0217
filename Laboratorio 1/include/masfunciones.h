#ifndef MASFUNCIONES_H 
#define MASFUNCIONES_H
#include "Tablero.h"

  /**
  * @brief Este metodo pide los nombres de los jugadores
  * @details Utiliza cin y devuevle la cantidad de jugadores
  */
int pedirnombre();
  /**
  * @brief Este metodo se encarga de ir  jugando
  * @details Dependiendo de la posicion de casilla en la que este ocurrira una accion
  */
bool turnos();

  /**
  * @brief Este metodo se encarga de revisar quien no tiene dinero
  */
bool BancaRota(Jugador*);
  /**
  * @brief Este metodo se encarga de revisar quien es el ganador
  * @param Jugador*: El puntero al jugador que podria estar en bancarrota
  * @details Si ya todos tienen menos de 0 colones y solo queda uno vivo, este sera el ganador
  */
bool Ganador();


#endif 
