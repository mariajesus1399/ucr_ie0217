#ifndef JUGADOR_H
#define JUGADOR_H
#include <iostream>
using namespace std;
class Jugador
{
    public:
        /**
         * @brief Constructor del objeto Jugador
         * 
         */
    Jugador();
    Jugador(string nombre_jugador);
         /**
         * @brief Constructor del objeto Jugador
         * 
         */
	~Jugador();
    string nombre_jugador;
    int plata;
    int posicion;
    int turno;
         /**
         * @brief Este metodo tira el dado
         * @details Utiliza valores aleatorios y se los asigna a cada jugador
         */
    int tirar_dado();
         /**
         * @brief Este metodo mueve al jugador a la posicion indicada
         * @details Utiliza el valor del dado, para mover al jugador a la casilla respectiva
         */
    int moverse();
      /**
         * @brief Este metodo brinda la posicion del jugador
         */
    int getposicion();
};

#endif