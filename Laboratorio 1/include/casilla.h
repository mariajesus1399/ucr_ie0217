#ifndef CASILLA_H 
#define CASILLA_H

#include <string>
#include <iostream>
#include "Jugador.h"
using namespace std;

class Casilla
{
public:
   string nombre;
   string tipo;
   int precio;
   int renta;
   bool estadocompra;
   Jugador* dueno;
       /**
         * @brief Constructor del objeto Casilla
         * 
         */
   
   
   Casilla(string nombre, string tipo, int precio, int renta);
      
     /**
         * @brief Destructor del objeto Casilla
         * 
         */

   ~Casilla();


};
#endif

