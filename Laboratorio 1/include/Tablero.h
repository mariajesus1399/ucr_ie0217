#ifndef TABLERO_H 
#define TABLERO_H
#include "Jugador.h"
#include "casilla.h"

using namespace std;
#include <string>
            

class Tablero
{
public:
   int numjugadores;
   /**
   * @brief Constructor del objeto Jugador
   * 
   */
   Tablero(int);
   /**
   * @brief Constructor del objeto Jugador
   * 
   */
   ~Tablero();

   Casilla* tablero[36];/*maximas casillas*/

   //cual posicion del tablero esta
   Jugador* jugadorarray; /*max jugadores*/
   /**
   * @brief Este metodo mueve al jugador
   * @details El metodo verifica si paso el inicio, si sucede llama al metodo PasarInicio, y guarda la posicion del jugador
   */
   int Mover(Jugador*); /*mueve*/
   /**
   * @brief Este metodo le da 200 al jugador al pasar por inicio
   * @details Le entrega dinero al jugador
   * @param Jugador*: El puntero del jugador que paso por el Inicio
   */
   void PasarInicio(Jugador*);
   /**
   * @brief Este metodo lse encarga de comprar propiedades
   * @param Propiedad* el puntero a la propiedad actual
   */
   void ComprarPropiedad(Jugador*,Casilla*);
   /**
   * @brief Este metodo se encarga de cobrar el impuesto
   * @param Jugador* el puntero del jugador que se encuentra en la casilla
   */
   void impcobrar(Jugador*);
   /**
   * @brief Este metodo coloca al jugador en la carcel
   * @details Le rebajara 200 por estar en la carcel
   * @param Jugador*: El puntero del jugador que va a la carcel
   */
   void irCarcel(Jugador*);
 
};

#endif
