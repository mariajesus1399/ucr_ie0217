#include "poli.h"
#include <vector>
// void Print(char name, const Poly &A) {
// 	cout << name << "(" << A.size()-1 << ") = [ ";
// 	copy(A.begin(), A.end(), ostream_iterator<decltype(A[0])>(cout, " "));
// 	cout << "]\n";
// }

Poli::Poli(){
    this->coeficiente2=0;
    this->coeficiente1=0;
    this->coeficiente0=0;
    this->coeficiente4=0;
    this->coeficiente3=0;

}

Poli::Poli(int coeficiente4, int coeficiente3,int coeficiente2, int coeficiente1, int coeficiente0){
    this->coeficiente4=coeficiente4;   
    this->coeficiente3=coeficiente3;
    this->coeficiente2=coeficiente2;
    this->coeficiente1=coeficiente1;
    this->coeficiente0=coeficiente0;    
}

Poli Poli::operator+ (Poli const &obj){
    Poli result;
    result.coeficiente2=(this->coeficiente2)+(obj.coeficiente2);
    result.coeficiente1=(this->coeficiente1)+(obj.coeficiente1);
    result.coeficiente0=(this->coeficiente0)+(obj.coeficiente0);
    return result;
}

Poli Poli::operator- (Poli const &obj){
    Poli result;
    result.coeficiente2=(this->coeficiente2)-(obj.coeficiente2);
    result.coeficiente1=(this->coeficiente1)-(obj.coeficiente1);
    result.coeficiente0=(this->coeficiente0)-(obj.coeficiente0);
    return result;
}

Poli Poli::operator * (Poli const &obj){
    Poli result;
    result.coeficiente4=(this->coeficiente2)*(obj.coeficiente2);
    result.coeficiente3=((this->coeficiente2)*(obj.coeficiente1))+((this->coeficiente1)*(obj.coeficiente2));
    result.coeficiente2=((this->coeficiente2)*(obj.coeficiente0))+((this->coeficiente1)*(obj.coeficiente1))+((this->coeficiente0)*(obj.coeficiente2));
    result.coeficiente1=((this->coeficiente1)*(obj.coeficiente0))+((this->coeficiente0)*(obj.coeficiente1));
    result.coeficiente0=(this->coeficiente0)*(obj.coeficiente0);
    return result;
}

Poli Poli::operator /(Poli const &obj){
  Poli result;
  int k=0-obj.coeficiente0;
  result.coeficiente1=this->coeficiente2;
  result.coeficiente0=k*this->coeficiente2 + this->coeficiente1;
  return result;
}
ostream& operator << (ostream& os, Poli  &obj){
    os << obj.coeficiente4<<"x^4+"<< obj.coeficiente3<<"x^3+"<<obj.coeficiente2 << "x^2+" << obj.coeficiente1<< "x+" << obj.coeficiente0 ;
    return os;
}



