#include <iostream>
#include "fracc.h"
#include "poli.h"

using namespace std;
int main(){

    cout<<"A continuacion se presentan las pruebas del programa:"<<endl;
    cout<<"Fracciones"<<endl;
    Fraccion f1(2,3);
    Fraccion f2(1,2);

    cout<<"Fraccion 1:"<<endl;
    cout<<f1<<endl;

    cout<<"Fraccion 2:"<<endl;
    cout<<f2<<endl;

    Fraccion f3 = f1+f2;
    Fraccion f4 = f1-f2;
    Fraccion f5 = f1*f2;
    Fraccion f6 = f1/f2;
    cout<<"Fraccion 1+Fraccion 2:"<<endl;
    cout<<f3<<endl;
    cout<<"Fraccion 1-Fraccion 2:"<<endl;
    cout<<f4<<endl;
    cout<<"Fraccion 1*Fraccion 2:"<<endl;
    cout<<f5<<endl;
    cout<<"Fraccion 1/Fraccion 2:"<<endl;
    cout<<f6<<endl;
   
    cout<<"--------------------------------------------------" <<endl;
    cout<<"Polinomios:"<<endl;

   
    Poli p1(0,0,1,1,-20);
    Poli p2(0,0,0, 1,5);

    cout<<"Polinomio 1:"<<endl;
    cout<<p1<<endl;
    cout<<"Polinomio 2:"<<endl;
    cout<<p2<<endl;
    cout<<"Los dos polinomios anteriores se utilizaran para el metodo de division"<<endl;
    Poli pp=p1/p2;
    cout<<"Polinomio1/Polonomio2:"<<endl;
    cout<<pp<<endl;

    Poli p3(0,0,2,2,9);
    Poli p4(0,0,4,-3,1);

    Poli p5=p3+p4;
    Poli p6=p3-p4;
    Poli p7=p3*p4;

    cout<<"Polinomio 3:"<<endl;
    cout<<p3<<endl;
    cout<<"Polinomio 4:"<<endl;
    cout<<p4<<endl;   
    cout<<"Polinomio 3+Polinomio 4:"<<endl;
    cout<<p5<<endl;
    cout<<"Polinomio 3-Polinomio 4:"<<endl;
    cout<<p6<<endl;
    cout<<"Polinomio 3*Polinomio 4:"<<endl;
    cout<<p7<<endl;








    
     


  return 0;
}
