#pragma once
//#include "cell.h"
//#include <string>
#include "field.h"
#include <iostream>

using namespace std;
class Cell;
class Unit{
  public:
   Unit();
   int id;
   char type;
   string name;
   int maxHitPoints;
   int hitPoints;
   int att;
   int defense;
   int range;
   int level;
   int experience;
   int movement;
   int posX;
   int posY;
   int cost;
   /**
   * @brief Este metodo se encarga de mover
   * @details Es virtual porque para cada tipo de unidad es diferente
   */
   virtual bool move(Cell* celda, int, int, Field* playfield) = 0;
   /**
    * @brief Este metodo se encarga de atacar
    * @details Es virtual porque para cada tipo de unidad es diferente
    */
   virtual void attack(Unit&other) = 0;
   /**
    * @brief Este metodo se encarga de revisar si un jugador pierde
    * @details Da puntos al jugador que vencio a la unidad
    */
   void revisarderrota(Unit&other);
   /**
  * @brief Este metodo despliega el estado de la unidad
  */
   void desplegarestado();
   /**
  * @brief Este metodo despliega el estado de la unidad enemiga
  */
   void desplegarestadoenemigo(Unit&other);
   /**
* @brief Brinda el tipo de unidad, para imprimir el campo
*/
   char gettipou();
   ~Unit();

};
