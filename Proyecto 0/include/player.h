#pragma once
#include "player.h"
#include "cavalry.h"
#include "archer.h"
#include "lancer.h"
#include <string>
#include <vector>

using namespace std;
class Field;
class Unit;
class Player{
    public:
      Player();
      int maxCost;
      vector <Unit*> army;
      Field* playfielddd;
      int score;
      /**
     * @brief Este metodo se encarga de armar el ejercito
     * @details Depende de los parametros ingresados por el usuario
     */
      void createArmy(int, int, int);
      /**
     * @brief Este metodo  es el que lleva el orden del juego
     * @details Depende de los parametros ingresados por el usuario
     */
      bool play(Player&other);
      /**
       * @brief Este metodo  es el que organiza el ejercito
       * @details Le pide al usuario que organice donde quiere sus posiciones
       */      
      void setArmy();

      ~Player();
};
