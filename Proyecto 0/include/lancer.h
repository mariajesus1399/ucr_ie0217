#pragma once

#include <string>
#include "unit.h"
#include "field.h"
#include "lancer.h"

using namespace std;

class Lancer : virtual public Unit{
    public:
        Lancer();
        /**
      * @brief Este metodo se encarga de mover al jugador por el campo
      * @details Dependiendo del jugador el movimiento es diferente
      */
        bool move(Cell* celda, int, int,  Field* playfield);
        /**
        * @brief Este metodo se encarga de atacar a otro
        * @details Recopila la informacion necesaria y le baja HP al enemigo
        */
        void attack(Unit&other);
        Field* playfieldd;
        ~Lancer();

};
