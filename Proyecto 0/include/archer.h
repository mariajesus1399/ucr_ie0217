#pragma once

#include <string>
#include "unit.h"
#include "field.h"

using namespace std;

class Archer : virtual public Unit{
    public:
        Archer();
        ~Archer();
        /**
        * @brief Este metodo se encarga de mover al jugador por el campo
        * @details Dependiendo del jugador el movimiento es diferente
        */
        bool move(Cell* celda, int, int,  Field* playfield);
        Field* playfieldd;
        /**
       * @brief Este metodo se encarga de atacar a otro
       * @details Recopila la informacion necesaria y le baja HP al enemigo
       */        
        void attack(Unit&other);
};
