#pragma once
//#include "unit.h"
#include <string>
using namespace std;
class Unit;
//class Field;
class Cell{
    public:
      Cell();
      /**
     * @brief Este metodo  imprimir la informacion de la celda
     */
      void printInfo();
      /**
    * @brief Este metodo se encarga de darle un atributo a la celda
    * @details Proviene de setUnit de playfield
    */
      void setUnitCell(Unit* uni);
      /**
      * @brief Este metodo se encarga de saber si hay unidad
      * @details Proviene de getUnit de playfield
      */
      Unit* getUnitCell();
      /**
     * @brief Este metodo se encarga de darle estado a la celda
     */
      void setEstadoCell();
      /**
     * @brief Este metodo se encarga de darle estado a la celda
     */
      bool EstadoCeldaCell();
      /**
    * @brief Este metodo se encarga de saber cual unidad hay en la celda
    * @details Proviene de gettipo de playfield
    */
      char gettipoc();

     ~Cell();
      bool passable;
      Unit* armyc;
};
