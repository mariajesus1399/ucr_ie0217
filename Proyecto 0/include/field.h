#pragma once
#include "cell.h"
#include <string>

using namespace std;
class Unit;
class Field{
    public:
      Field();
      Cell* playfield[5][5];
      /**
     * @brief Este metodo se encarga de imprimir el campo
     */
      void printCampo();
      /**
     * @brief Este metodo se encarga de poner la unidad correspondiente
     */
      void setUnit(Unit* unidad, int, int);
      /**
     * @brief Este metodo se encarga de obtener el tipo de unit
     * @details Luego necesitara de un metodo de cell
     */
      void gettipof();
      /**
     * @brief Este metodo se encarga de darle el estado a la celda
     */
      bool EstadoCelda(int, int);
      /**
     * @brief Este metodo se encarga de obtener la unidad
     */
      Unit* getUnit(int, int);
        /**
         * @brief Este metodo se encarga de asignarle el estado a la celda correspondiente
         */
      void setEstado(int, int);
     ~Field();
};
