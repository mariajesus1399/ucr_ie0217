
#include "cell.h"
#include "unit.h"
#include <iostream>

using namespace std;

Cell::Cell(){
    this->passable=true;
}

void Cell::printInfo(){
  if(passable==false){
    cout<< "El terreno no es franqueable" << endl;
    cout << " Hay unidad en esta celda" << endl;
  }
  else{
    cout<< "El terreno es franqueable" << endl;

  }
}

void Cell::setUnitCell(Unit* uni){
  this->armyc=uni;
  this->passable=false;
}

Unit* Cell::getUnitCell(){
  return armyc;
}

void Cell::setEstadoCell(){
   this->passable=true;
   this->armyc=NULL;
}


bool Cell::EstadoCeldaCell(){
    if(this->passable==true){
      return true;
}
    else {
        cout<<"Celda ocupada, elija otro lugar"<<endl;
        return false;
    }
}

 char Cell::gettipoc(){
   if(this->passable==true){
      return '-';
    }
   else {
      return this->armyc->gettipou();
   }
 }

Cell::~Cell(){

}
