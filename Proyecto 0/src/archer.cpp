
#include "archer.h"
#include "unit.h"
#include "cell.h"
#include "player.h"
#include <string>
#include "field.h"
#include <iostream>

using namespace std;

Archer::Archer(){
    this->id=0;
    this->type='A';
    this->name="Archer";
    this->maxHitPoints=15;
    this->hitPoints=15;
    this->att=5;
    this->defense=5;
    this->range=3;
    this->level=1;
    this->experience=0;
    this->movement=1;
    this->posX=0;
    this->posY=0;
    this->cost=4;
}

bool Archer::move(Cell* celda, int xprox, int yprox, Field* playfieldd){
    if (celda->passable==0){
      cout<<"No se puede mover, casilla ocupada"<<endl;
      return false;
    }
        else{
        this->posX=xprox;
        this->posY=yprox;
        celda->passable=false;
        cout<<"Su personaje se movió correctamente"<<endl;
        return true;
        }
    }

void Archer::attack(Unit&other){
    int enemy_df;
    int damage;
    enemy_df=other.defense;
    damage=this->att-enemy_df;
    if (damage<=0){
        cout<<"El ataque no fue efectivo"<<endl;
    }
    else{
    cout<<"El ataque fue efectivo"<<endl;
    other.hitPoints=other.hitPoints-damage;
    }

}
Archer::~Archer(){
}
