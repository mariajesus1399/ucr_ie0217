
#include "lancer.h"
#include <string>
#include "unit.h"
#include "cell.h"
#include "player.h"
#include "field.h"
#include <iostream>

using namespace std;

Lancer::Lancer(){
    this->id=0;
    this->type='L';
    this->name="Lancer";
    this->maxHitPoints=20;
    this->hitPoints=20;
    this->att=7;
    this->defense=7;
    this->range=1;
    this->level=1;
    this->experience=0;
    this->movement=1;
    this->posX=0;
    this->posY=0;
    this->cost=3;
}

bool Lancer::move(Cell* celda, int xprox, int yprox, Field* playfield){
    if (celda->passable==0){
      cout<<"No se puede mover, casilla ocupada"<<endl;
      return false;

    }
        else{
        this->posX=xprox;
        this->posY=yprox;
        cout<<"Su personaje se movió correctamente"<<endl;
        celda->passable=false;
        return true;
        }
}



void Lancer::attack(Unit&other){
  int enemy_df;
  int damage;
  enemy_df=other.defense;
  damage=this->att-enemy_df;
  if (damage<=0){
      cout<<"El ataque no fue efectivo"<<endl;
  }
  else{
    cout<<"El ataque fue efectivo"<<endl;
    other.hitPoints=other.hitPoints-damage;
  }

}
Lancer::~Lancer(){
}
