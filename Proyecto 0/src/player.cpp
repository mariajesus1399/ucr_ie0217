#include "player.h"
#include "lancer.h"
#include <string>
#include <iostream>

using namespace std;


Player::Player(){

}

void Player::createArmy(int number_lancer, int number_calvary, int number_archer){
      for(int i=0; i<number_lancer; i++){
         army.push_back(new Lancer());

      }
      for(int i=0; i<number_calvary; i++){

          army.push_back(new Cavalry());
      }
      for(int i=0; i<number_archer; i++){
         army.push_back(new Archer());

      }
}

void Player::setArmy(){
   int a;
   int b;
   bool ocupada;
   for(size_t j = 0; j < army.size(); j++){
   army[j]->id = j;
   int ciclo = 0;
      while (ciclo == 0){
      cout<<"Acomode su figura numero: " << j+1 <<endl;
      cout<<army[j]->name<<endl;
      cout<<"Ingrese la posición x: "<<endl;
      cin>>a;
      cout<<"Ingrese la posición y: "<<endl;
      cin>>b;
      ocupada = playfielddd->EstadoCelda(a,b);
      if (ocupada==1){
        playfielddd->setUnit(army[j],a,b);
        army[j]->posX=a;
        army[j]->posY=b;
        cout<<"Su personaje se está acomodando"<<endl;
        ciclo = 1;
      }
      else
      {
        cout<< "Posición ocupada, acomode su figura de nuevo" <<endl;
        ciclo = 0;
      }
    }
   }
}



bool Player::play(Player&other){
  if (army.size()==0)
  {
    cout<< "El jugador contrario ha ganado la partida" <<endl;
    return false;
  }
  else if(other.army.size()==0){
    cout<< "Usted ha ganado la partida, felicidades"<<endl;
    return false;
  }
  else {
  char m;
  int proxx;
  int proxy;
  int proxax;
  int proxay;
  char a;
  char b;
  int d;
  int f;

  for (size_t j = 0; j < army.size(); j++){
     cout<<"Su unidad es " << army[j]->name << endl;
     cout<<"El estado actual de su unidad es"<<endl;
     army[j]->desplegarestado();
     cout<<"¿Desea moverse?"<<endl;
     cin>>m;
     if (m=='y'){
        cout<<"¿Dónde?"<<endl;
        cout<<"x: "<<endl;
        cin>>proxx;
        cout<<"y: "<<endl;
        cin>>proxy;
        d=army[j]->posX;
        f=army[j]->posY;
        bool p;

        p = army[j]->move(playfielddd->playfield[proxx][proxy], proxx, proxy, playfielddd);
        if (p==false){

        }
        else{
         playfielddd->setEstado(d,f);

         playfielddd->setUnit(army[j],proxx,proxy);
       }

       cout<<"¿Desea atacar?"<<endl;
       cout<<"Nota: elija coordenadas según el ataque de su jugador"<<endl;
       cin>>b;
       if (b=='y'){

        cout<<"¿Dónde?"<<endl;
        cout<<"x: "<<endl;
        cin>>proxax;
        cout<<"y: "<<endl;
        cin>>proxay;
        Unit* enemy=playfielddd->getUnit(proxax,proxay);
        army[j]->attack(*enemy);
        army[j]->desplegarestadoenemigo(*enemy);
        for(size_t k = 0; k < other.army.size(); k++){
           if(other.army[k]->hitPoints<=0){

              other.army.erase(other.army.begin()+k);
              playfielddd->setEstado(other.army[k]->posX,other.army[k]->posY);
              cout<<"Ha eliminado a su contricante"<<endl;
           }
          else{
           }

        }
        army[j]->revisarderrota(*enemy);






     }
      else{
      cout<<"Se movió, pero no ataco"<<endl;
     }

     }
    else{
       cout<<"¿Desea atacar?"<<endl;
       cin>>a;
       if (a=='y'){
        cout<<"¿Dónde?"<<endl;
        cout<<"x: "<<endl;
        cin>>proxax;
        cout<<"y: "<<endl;
        cin >>proxay;
        Unit* enemy=playfielddd->getUnit(proxax,proxay);
        army[j]->attack(*enemy);
        army[j]->desplegarestadoenemigo(*enemy);

        army[j]->revisarderrota(*enemy);
          for(size_t k = 0; k < other.army.size(); k++){
           if(other.army[k]->hitPoints<=0){

              other.army.erase(other.army.begin()+k);
              playfielddd->setEstado(other.army[k]->posX,other.army[k]->posY);
              cout<<"Ha eliminado a su contricante"<<endl;
           }
          else{
           }

        }
     }
       else {
          cout<<"No se movió, ni atacó"<<endl;
       }
    }

   }
}
return true;
}

Player::~Player(){

}
