#include "cavalry.h"
#include <string>
#include "unit.h"
#include "cell.h"
#include "player.h"
#include "field.h"
#include <iostream>

using namespace std;

Cavalry::Cavalry(){
    this->id=0;
    this->type='C';
    this->name="Cavalry";
    this->maxHitPoints=25;
    this->hitPoints=25;
    this->att=15;
    this->defense=5;
    this->range=1;
    this->level=1;
    this->experience=0;
    this->movement=3;
    this->posX=0;
    this->posY=0;
    this->cost=5;
}
bool Cavalry::move(Cell* celda, int proxx, int proxy, Field* playfieldd){
    if (celda->passable==0){
              cout<<"No se puede mover, casilla ocupada"<<endl;
      return false;

    }
        else {

         if(proxx==this->posX){
             if(proxy<this->posY)
             {
             if(
               playfieldd->playfield[this->posX][this->posY-1]->passable==false){ //posible error
                 return false;
             }
                 else{
                     if(playfieldd->playfield[this->posX][this->posY-2]->passable==false){
                         return false;
                     }
                     else{
                         this->posX=proxx;
                         this->posY=proxy;
                         celda->passable=true;
                         return true;
                     }
                 }
             }
             else{
                 if(playfieldd->playfield[this->posX][this->posY+1]->passable==false){ //posible error
                 return false;
             }
                 else{
                     if(playfieldd->playfield[this->posX][this->posY+2]->passable==false){
                         return false;
                     }
                     else{
                         this->posX=proxx;
                         this->posY=proxy;
                         celda->passable=true;
                         return true;
                     }
                 }
             }

             }
             else{
             if(proxx<this->posY)
             {
             if(playfieldd->playfield[this->posX-1][this->posY]->passable==false){ //posible error
                 return false;
             }
                 else{
                     if(playfieldd->playfield[this->posX-2][this->posY]->passable==false){
                         return false;
                     }
                     else{
                         this->posX=proxx;
                         this->posY=proxy;
                         celda->passable=true;
                         return true;
                     }
                 }
             }
             else{
                 if(playfieldd->playfield[this->posX+1][this->posY]->passable==false){ //posible error
                 return false;
             }
                 else{
                     if(playfieldd->playfield[this->posX+2][this->posY]->passable==false){
                         return false;
                     }
                     else{
                         this->posX=proxx;
                         this->posY=proxy;
                         celda->passable=true;
                         return true;
                     }
                 }
             }

             }

             }

        }


void Cavalry::attack(Unit&other){
    int enemy_df;
    int damage;
    enemy_df=other.defense;
    damage=this->att-enemy_df;
    if (damage<=0){
        cout<<"El ataque no fue efectivo"<<endl;
    }
    else{
    cout<<"El ataque fue efectivo"<<endl;
    other.hitPoints=other.hitPoints-damage;
    }

}
Cavalry::~Cavalry(){
}
